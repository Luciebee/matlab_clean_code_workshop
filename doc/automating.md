# Automating

## Pre-commit-CI

!!! warning

    Only works on github repositories.

## CI/CD in gitlab

### Setting up a CI/CD pipeline

!!! tips

    Create scripts that you can run locally and in CI.
